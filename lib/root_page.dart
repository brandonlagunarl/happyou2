import 'package:flutter/material.dart';
import 'package:happyou2/home_page.dart';
import 'login_page.dart';
import 'auth.dart';
//Brandon Laguna RL 4:40am 8/21/2019 
//Last Update 9/16/2019
class RootPage extends StatefulWidget {
  RootPage({this.auth });
  final BaseAuth auth;
  @override
  _RootPageState createState() => new _RootPageState();
}

enum AuthStatus{
  notSignedIn,
  signedIn
}

class _RootPageState extends State<RootPage> {

  AuthStatus _authStatus = AuthStatus.notSignedIn;

  @override
  void initState() {
    super.initState();
    widget.auth.currentUser().then((userId){
      setState(() {
        //set estate of signed or not signed *comment only for develop login page
       _authStatus = userId == null? AuthStatus.notSignedIn : AuthStatus.signedIn; 
      });
    });
  }

  void _signedIn(){
    setState(() {
      _authStatus = AuthStatus.signedIn;
    });
  }
  void _signedOut(){
    setState(() {
      _authStatus = AuthStatus.notSignedIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    switch (_authStatus) {
      case AuthStatus.notSignedIn:
          return new LoginPage(auth: widget.auth, onSignedIn: _signedIn);

      break;

      case AuthStatus.signedIn:
          return new HomePage(
            auth: widget.auth,
            onSignedOut: _signedOut,
          );
      break;
      
    }
    
  }
}