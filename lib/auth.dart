import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'dart:async';
//Brandon Laguna RL 4:04am 8/21/2019 
//Last Update 9/16/2019 
//You can create an algorhitm to create or login an account with different platform
abstract class BaseAuth{
  Future<String> signInWithEmailAndPassword(String email, String password);
  Future<String> createUserWithEmailAndPassword(String email, String password);
  Future<String> signInWithGoogle();
  Future<String> currentUser ();
  Future<void> signOut();

}
 
class Auth implements BaseAuth{
      //start firebase auth instance to use later on authentications
    final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
    final GoogleSignIn _googlSignIn = new GoogleSignIn();

  //Sign In With Email And Password *Implementation
  Future<String> signInWithEmailAndPassword(String email, String password) async{
    AuthResult user = await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
    FirebaseUser usr = user.user;
    return usr.uid;
  }
  //Create an account With Email And Password *Implementation
  Future<String> createUserWithEmailAndPassword(String email, String password) async{
  AuthResult user = await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);
  FirebaseUser usr = user.user;
  return usr.uid;
  }
  Future<String> signInWithGoogle()  async{
    final GoogleSignInAccount googleUser = await _googlSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =await googleUser.authentication;
    final AuthCredential credential = GoogleAuthProvider.getCredential(accessToken: googleAuth.accessToken,idToken: googleAuth.idToken);
    AuthResult result = await _firebaseAuth.signInWithCredential(credential);
    FirebaseUser usr = result.user;
    return usr.uid;
  }

  //Detect te current User logged
  Future<String> currentUser () async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    return user.uid;
  }
  //Logout from session
  Future<void> signOut() async{
      return _firebaseAuth.signOut();
  }
}