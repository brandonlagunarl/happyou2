import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'auth.dart';
final String google_logo ='assets/graphic/logo/google_logo.svg';
final String facebook_logo ='assets/graphic/logo/facebook_logo.svg';
class LoginPage extends StatefulWidget {
  LoginPage({this.auth, this.onSignedIn});
  final BaseAuth auth;
  final VoidCallback onSignedIn;
  @override
  _LoginPageState createState() => new _LoginPageState();
}

enum FormType { login, register, googleSignIn, facebookSignIn }

class _LoginPageState extends State<LoginPage> {
  static FirebaseAnalytics analytics = new FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      new FirebaseAnalyticsObserver(analytics: analytics);

  final formKey = new GlobalKey<FormState>();
  String _email;
  String _password;
  FormType _formType = FormType.login;

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {
      try {
        if (_formType == FormType.login) {
          String userId =
              await widget.auth.signInWithEmailAndPassword(_email, _password);
              print("signed as $userId");
        }else {
          String userId = await widget.auth
              .createUserWithEmailAndPassword(_email, _password);
              print("Registered as $userId");
        }
        widget.onSignedIn();
      } catch (e) {
        print("Error: $e");
        showDialog(
          context: context,
          builder:(BuildContext context){
            return alerSignError;
          }
        );
      }
    }
  }

  Future signInWithoutValidate() async {
    try {
      if(_formType == FormType.googleSignIn){
            String userId = await widget.auth.signInWithGoogle();
            print("google signIn as $userId");
            widget.onSignedIn();
      }else if(_formType == FormType.facebookSignIn){
            String userId = await widget.auth.signInWithGoogle();
            print("Facebook signIn as $userId");
            widget.onSignedIn();
      }
        else{}
    } catch (e) {
      print(e);
    }
  }

  void moveToRegister() {
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.register;
    });
  }

  void moveToLogin() {
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.login;
    });
  }
  void signInGoogle(){
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.googleSignIn;
    });
    signInWithoutValidate();
  }
  void signInFacebook(){
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.facebookSignIn;
    });
    signInWithoutValidate();
  }

  var alerSignError = AlertDialog(
          title: Text("Ups!"),
          content: Text("Credenciales incorrectas"),
        );

        
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding:false,
      body: new Column(
        children: [
            Container(
              child: new Form(
              key: formKey,
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: loginHeader() + builImputs() + buildSubmitButtons() + googleSignin()+ facebookSignin(),
              ),
            ),
            ),
        ]
        ),
    );
  }


  List<Widget> loginHeader(){
    return [
      Container(
        decoration: new BoxDecoration(
          borderRadius:  new BorderRadius.only(
            bottomLeft: const Radius.circular(70.0),
            bottomRight: const Radius.circular(00.0),
          ),
          color: Color(0xffEB2024),
          ),
        height: 140.0,
         child: Center(
        
      ),
      ),
    ];
  }
  List<Widget> googleSignin(){
    return [
      new Container(
            padding: EdgeInsets.only(
              left: 40.0,
              right: 40.0
            ),
            width: 250.0,
            child: Align(
            alignment: Alignment.center,
            child: RaisedButton(
              padding: EdgeInsets.all(11.0),
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(4.0)),
                color: Color(0xffffffff),
                child: Row(
                  children: <Widget>[
                  SvgPicture.asset(google_logo,height: 22.0),
                  SizedBox(width:10.0),
                  Text(
                  'Inicia con Google',
                  style: TextStyle(color: Color(0xff808080), fontSize: 18.0,fontWeight:FontWeight.w600),
                  ),
                ],),
                onPressed: () => signInGoogle(),
                ),
                )
          ),
    ];
  }

  List<Widget> facebookSignin(){
    return [
      new Container(
            padding: EdgeInsets.only(
              left: 40.0,
              right: 40.0
            ),
            width: 250.0,
            child: Align(
            alignment: Alignment.center,
            child: RaisedButton(
              padding: EdgeInsets.all(11.0),
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(4.0)),
                color: Color(0xffffffff),
                child: Row(
                  children: <Widget>[
                  SvgPicture.asset(facebook_logo,height: 22.0),
                  SizedBox(width:10.0),
                  Text(
                  'Inicia con Facebook',
                  style: TextStyle(color: Color(0xff808080), fontSize: 18.0,fontWeight:FontWeight.w600),
                  ),
                ],),
                onPressed: () => signInFacebook(),
                ),
                )
          ),
    ];
  }

  List<Widget> builImputs() {
    //Construcion of Login Form
    return [
      Container(
        padding: EdgeInsets.only(
          top: 40.0,
          left:20.0,
          right: 20.0
        ),
        child: Column(
          children:[
            new Container(
              child: new TextFormField(
              style: new TextStyle(fontSize: 11.0),
              decoration: new InputDecoration(disabledBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(5.0)),borderSide: BorderSide(color: Colors.black)), border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(50.0))),hintText: "Correo",icon: Icon(Icons.accessibility_new)),
              validator: (value) => value.isEmpty ? "Email can\'t be empty" : null,
              onSaved: (value) => _email = value,
            )
            ),
            Container(
              padding:EdgeInsets.only(
                top: 10.0
              ),
              child: new TextFormField(
              style: new TextStyle(fontSize: 11.0),
              decoration: new InputDecoration(disabledBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(5.0)),borderSide: BorderSide(color: Colors.black)), border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(50.0))),hintText: "Contraseña", icon: Icon(Icons.vpn_key)),
              obscureText: true,
              validator: (value) => value.isEmpty ? "Password can\'t be empty" : null,
              onSaved: (value) => _password = value,
            ),
            ),
            
          ]
        ),
      ),
      
    ];
  }

  List<Widget> buildSubmitButtons() {
    if (_formType == FormType.login) {
      return [
        Container(
          padding: EdgeInsets.only(
            left: 20.0,
            right: 20.0
          ),
          child: new RaisedButton(
          shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),color: Color(0xffEB2024),
          child: new Text(
            "Login",
            style: new TextStyle(fontSize: 15.0,color: Color(0xffffffff)),
          ),
          onPressed: validateAndSubmit,
        ),
        ),
        Container(
          
          child: new FlatButton(
          child: new Text(
            "Create an account",
            style: new TextStyle(fontSize: 15.0,color: Color(0xffEB2024)),
          ),
          onPressed: moveToRegister,
        ),
        )
      ];
    } else {
      return [
        new RaisedButton(
          shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),color: Color(0xffEB2024),
          child: new Text(
            "Create an account",
            style: new TextStyle(fontSize: 15.0,color: Color(0xffffffff)),
          ),
          onPressed: validateAndSubmit,
        ),
        new FlatButton(
          
          child: new Text(
            "Have an account? Login",
            style: new TextStyle(fontSize: 15.0,color: Color(0xffEB2024)),
          ),
          onPressed: moveToLogin,
        ),
      ];
    }
  }
}
