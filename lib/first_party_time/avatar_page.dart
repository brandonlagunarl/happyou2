import 'package:flutter/material.dart';

class AvatarPage extends StatefulWidget {
  @override
  _AvatarPageState createState() => _AvatarPageState();
}

class _AvatarPageState extends State<AvatarPage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        child: new Center(
          child: new Column(
            children: <Widget>[

            ],
          ),
        ),
      ),
    );
  }

  Widget _buildInputForm(){
    return new Form(
      child: new Column(
        children: <Widget>[
          new TextFormField(
            decoration: new InputDecoration(
              labelText: 'Ingresa tu nombre,',
              labelStyle: new TextStyle(fontSize: 20.0,)
              ),
              style: new TextStyle(fontSize: 24.0, color: Colors.black),
              validator: (val) => val.isEmpty? 'El nombre no puede esar vacio' : null,
          ),
        ],
      ),
    );
  }
}